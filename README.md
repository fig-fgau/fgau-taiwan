# FGAU Taiwan

This repository is the home of custom scenery for the Republic of China (Taiwan).  It was created by Zheng Yuyan of the FGDG - FlightGear Development Group.  The project is currently hosted by FlightGear Australia (FGAU), but was not created by FGAU.

-------------------------------------------------------

©️ 2019-2020, Zheng Yuyan (FGDG)
©️ 2017-2016, Israel Hernandez (IAHM-COL)
©️ 2017-2016, Peter Brendt (Jabberwocky)
©️ 2017-2016, Sharath Mahadevan (SHM)
©️ 2017 Glen D'souza
©️ 2017-2016, J. Maverick
©️ 2017-2016, Martin Herweg (D-LASER)
©️ 2017-2016, Terrasync's FlightGear Scenery Database objects authors
©️ 2016, John Statto (Custom Landclasses)

License: GPL 2+

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.